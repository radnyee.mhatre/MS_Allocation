import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddGradComponent} from './components/add-grad/add-grad.component';
import {EditGradComponent} from './components/edit-grad/edit-grad.component';
import {ViewGradComponent} from './components/view-grad/view-grad.component';
import {SkillComponent} from './components/skill/skill.component';
import {DisplayDetailsComponent} from  './components/display-details/display-details.component';
import {EditSkillsComponent} from './components/edit-skills/edit-skills.component';
import {TrendsComponent} from './components/trends/trends.component';
import {GradHistoryComponent} from './components/grad-history/grad-history.component'

const routes: Routes = [
  { path: '', component: ViewGradComponent}, 
    {path : 'addGrad', component : AddGradComponent  },
    {path : 'viewGrad', component : ViewGradComponent  },
    {path : 'editGrad/:id', component : EditGradComponent  },
    {path : 'skills', component : SkillComponent  },
    {path : 'displayDetails/:id', component : DisplayDetailsComponent },
    {path : 'editSkills/:id', component : EditSkillsComponent },
    {path :'gradHistory/:id', component:GradHistoryComponent},
     {path : 'trends', component : TrendsComponent }
 ]
;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CandidateManagementRoutingModule { }
