export class GradDetails {
    public createdBy: String;
    public createdOn: Date;
    public gradId: number;
    public gradFirstName: String;
    public gradMiddleName: String;
    public gradLastName: String;
    public gender: String;
    public email: String;
    public contact: number;
    public houseNo: String;
    public street: String;
    public city: String;
    public state: String;
    public zipcode: String;
    public institute: String;
    public degree: String;
    public branch: String;
    public joiningDate: Date;
    public joiningLocation: String;
    public feedback: String;
    public skills: [];

    constructor(createdBy, createdOn, updatedBy, updatedOn, gradId, gradFirstName,
        gradMiddleName, gradLastName, gender, email, contact, houseNo,
        street, city, state, zipcode, institute, degree, branch,
        joiningDate, joiningLocation, feedback, skills) {
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.gradId = gradId;
        this.gradFirstName = gradFirstName;
        this.gradMiddleName = gradMiddleName;
        this.gradLastName = gradLastName;
        this.gender = gender;
        this.email = email;
        this.contact = contact;
        this.houseNo = houseNo;
        this.street = street;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.institute = institute;
        this.degree = degree;
        this.branch = branch;
        this.joiningDate = joiningDate;
        this.joiningLocation = joiningLocation;
        this.feedback = feedback;
        this.skills = skills;
    }
}