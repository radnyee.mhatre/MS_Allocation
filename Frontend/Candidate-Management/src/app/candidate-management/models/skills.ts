export class skill {
    public skillId: number;
    public skillName: string;
    public createdBy: String;
    public createdOn: String;

    constructor(id, skill, createdOn, createdBy) {
        this.skillId = id,
            this.skillName = skill,
            this.createdOn = createdOn,
            this.createdBy = createdBy
                }
}