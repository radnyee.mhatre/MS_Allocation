import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SkillService } from '../../providers/skill.service'

@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.css']
})
export class SkillComponent implements OnInit {

  constructor(
    private skillService: SkillService,
  ) { }
  skills:any;
  newSkill=new FormControl();
  msg:String;
  ngOnInit() {
    this.skillService.getSkills().subscribe((data) => {
      console.log(data);
      this.skills=data;
    });

  }
checkSkillExists()
{
  for(let skill of this.skills)
  {
    if(skill.skillName.toLowerCase() == this.newSkill.value.toLowerCase() )
    return true;
  }
  return false
}
  addSkill(){
    this.checkSkillExists()? this.msg="Skill already Exists":
    this.skillService.addSkill(this.newSkill.value)
    .subscribe((data)=>{
      console.log(data);
      this.skills=data;
      this.msg="Skill Added successfully"
    })
  }
}
