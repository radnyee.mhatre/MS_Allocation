import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { skill } from '../../models/skills';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SkillService } from '../../providers/skill.service';

@Component({
  selector: 'app-edit-skills',
  templateUrl: './edit-skills.component.html',
  styleUrls: ['./edit-skills.component.css']
})
export class EditSkillsComponent implements OnInit {

  constructor(private route: ActivatedRoute,private router: Router,private skillsService: SkillService) { }
  newSkill:any;
  dataError: boolean = true;
  skills:any=[];
  skillsList:any;
  allSkills:any;
  skillList:any=[];
  msg:string;
  id:number;
  addSkillSet:boolean=false;
  newSkillSet=new FormControl();
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
     // console.log(this.id);
    });

      
    //console.log(history.state.data);
    if (history.state.data) {
      this.dataError = false;
      this.skills = history.state.data.skills;
      //console.log(this.skills);
      this.skillsService.getSkills().subscribe((data) => {
        //console.log(data)
        this.skillsList = data;
        this.allSkills=data
        //console.log("skillList",this.allSkills)
      })
    }

  }
   contains(newSkill){
     
     //console.log("ll",newSkill)
     for(let i=0;i<this.skills.length;i++)
     {
      //console.log(this.skills[i]);
      if(this.skills[i].skillID==newSkill.skillID)
      return true;
     }
    return false;
  }
addskill() {

    //console.log("adding",this.newSkill)
  //console.log(this.skills)
    if (this.newSkill != undefined && this.newSkill.skillName != '') {
      if ((!this.contains(this.newSkill))&&(!this.skillList.includes(this.newSkill)))
        this.skillList.push(this.newSkill);
    }
}
removeskill(skill) {
  //console.log(skill)
  this.skillList.splice(skill, 1)
}
updateSkills()
{
  this.skillsService.updateSkills(history.state.data.gradId,this.skillList)
  .subscribe((data)=>{
    //console.log(data);
    this.router.navigate(["/candidate-management/editGrad",this.id]);
  })
}
checkSkillExists()
{
  for(let skill of this.allSkills)
  {
    if(skill.skillName.toLowerCase() == this.newSkillSet.value.toLowerCase() )
    return true;
  }
  return false
}
  addNewSkill(){
    this.checkSkillExists()? this.msg="Skill already Exists":
    this.skillsService.addSkill(this.newSkillSet.value)
    .subscribe((data)=>{
      //console.log(data);
      this.allSkills=data;
      this.skillsList=data;
      this.msg="Skill Added successfully";
      this.addSkillSet=false;
    })
  }
  updateSkillsSet(){
    this.addSkillSet=true;
  }
}
