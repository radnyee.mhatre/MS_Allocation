import { Component, OnInit, ViewChild } from '@angular/core';
import { GradService } from '../../providers/grad.service';
import { GradDetails } from '../../models/GradDetails';
import { MatTableDataSource } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { DeteteGradService } from '../../providers/detete-grad.service'

@Component({
  selector: 'app-view-grad',
  templateUrl: './view-grad.component.html',
  styleUrls: ['./view-grad.component.css']
})
export class ViewGradComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  constructor(private gradService: GradService, private deleteGradService: DeteteGradService,
     private router: Router,private route:ActivatedRoute) { }
  grads:any;
  datasource:any;
  
  // datasource;
  displayedColumns: string[] = ['gradId', 'gradFirstName', 'gradLastName', 'email', 'institute', 'joiningLocation', 'createdOn'];
  ngOnInit() {
    
    this.gradService.getGrads().subscribe((data) => {
      console.log(data)
      this.grads = data;
      this.datasource = new MatTableDataSource<GradDetails>(this.grads);
      this.datasource.paginator = this.paginator;
      this.datasource.sort = this.sort;
      console.log(this.datasource);
    })
  }

  editDetails(data) {
    this.router.navigate(['/candidate-management/editGrad',data.gradId]);
  }

  viewDetails(data) {
    this.router.navigate(['/candidate-management/displayDetails',data.gradId] );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;

    this.datasource.filter = filterValue.trim().toLowerCase();
    console.log()
  }
  delete(element) {
    this.deleteGradService.deleteGrad(element.gradId).subscribe((data=>
      {
        this.grads=data
        this.datasource = new MatTableDataSource<GradDetails>(this.grads);
      }));
      this.router.navigate(["/candidate-management/viewGrad"]);
  }
}
