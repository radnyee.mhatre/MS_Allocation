import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'; 
import {GradService} from '../../providers/grad.service';
@Component({
  selector: 'app-display-details',
  templateUrl: './display-details.component.html',
  styleUrls: ['./display-details.component.css']
})
export class DisplayDetailsComponent implements OnInit {
id:number;

  constructor(private route: ActivatedRoute , private router: Router,
    private gradService:GradService) { }
 
    // In a real app: dispatch action to load the details here.
 
  data:any;
  showError=true;
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

      this.gradService.getGrad(this.id).subscribe((data)=>
      {
        this.data=data;
      });
      
  }
  viewAll()
  {
    this.router.navigate(['/candidate-management/viewGrad']);
  }
  editDetails()
  {
    this.router.navigate(['/candidate-management/editGrad',this.id]);
  }
  showHistory(){
    this.router.navigate(['/candidate-management/gradHistory',this.id]);
  }
}
