import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GradService } from '../../providers/grad.service'

@Component({
  selector: 'app-grad-history',
  templateUrl: './grad-history.component.html',
  styleUrls: ['./grad-history.component.css']
})
export class GradHistoryComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router,
    private gradService: GradService) { }
  id: number;
  gradData: any;
  ngOnInit() {

    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });
    this.gradService.getHistory(this.id).subscribe((data) => {
      this.gradData = data
    })

  }
  viewcurrent() {
    this.router.navigate(["/candidate-management/displayDetails", this.id])
  }
}
