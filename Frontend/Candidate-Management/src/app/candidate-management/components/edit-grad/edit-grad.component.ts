import { Component, OnInit } from '@angular/core';
import { GradDetails } from '../../models/GradDetails';
import { SkillService } from '../../providers/skill.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GradService } from '../../providers/grad.service';
import { Router ,ActivatedRoute} from '@angular/router'; 

@Component({
  selector: 'app-edit-grad',
  templateUrl: './edit-grad.component.html',
  styleUrls: ['./edit-grad.component.css']
})
export class EditGradComponent implements OnInit {

  constructor(private route: ActivatedRoute , private router: Router,
    private gradService:GradService) { }

  dataError:boolean=true;
  data:any;
  id:number;
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      console.log(this.id);
    });

      this.gradService.getGrad(this.id).subscribe((data)=>
      {
        //console.log(data);
        this.data=data;
        this.dataError=false;
        this.gradFirstName = new FormControl(this.data.gradFirstName, [Validators.required, Validators.maxLength(50)]);
        this.gradMiddleName = new FormControl(this.data.gradMiddleName, [Validators.required, Validators.maxLength(50)]);
        this.gradLastName = new FormControl(this.data.gradLastName, [Validators.required, Validators.maxLength(50)]);
        this.email = new FormControl(this.data.email, [Validators.required, Validators.email, Validators.maxLength(255)]);
        this.houseNo = new FormControl(this.data.houseNo, [Validators.required, Validators.maxLength(50)]);
        this.street = new FormControl(this.data.street, [Validators.required, Validators.maxLength(50)]);
        this.city = new FormControl(this.data.city, [Validators.required, Validators.maxLength(50)]);
        this.state = new FormControl(this.data.state, [Validators.required, Validators.maxLength(50)]);
        this.zipcode = new FormControl(this.data.zipcode, [Validators.required]);
        this.contact = new FormControl(this.data.contact, [Validators.required]);
        this.gender = new FormControl(this.data.gender, [Validators.required]);
      
        this.institute = new FormControl(this.data.institute, [Validators.required, Validators.maxLength(50)]);
        this.degree = new FormControl(this.data.degree, [Validators.required, Validators.maxLength(50)]);
        this.branch = new FormControl(this.data.branch, [Validators.required, Validators.maxLength(20)]);
      
        this.dateOfJoining = new FormControl(this.data.joiningDate, [Validators.required]);
        this.joiningLocation = new FormControl(this.data.joiningLocation, [Validators.required, Validators.maxLength(50)]);
        this.newSkill= new FormControl(-1,[Validators.required ,Validators.maxLength(50)]);
      });
   
  }

  retry()
  {
    window.location.href="/candidate-management/viewGrad";
  }
  skillsList:any;
  newSkill:any;

  inputError: boolean = true;
  errorMsg: String = undefined;
  progress = 0;
  skills = [];
  personalDetails: boolean = false;
  educationalDetails: boolean = true;
  professionalDetails: boolean = true;
  submitform: boolean = true;
  gradFirstName:any;
  gradMiddleName:any;
  gradLastName :any;
  email:any;
  houseNo:any ;
  street:any;
  city:any;
  state :any;
  zipcode:any;
  contact:any;
  gender :any;

  institute :any;
  degree:any ;
  branch :any;

  dateOfJoining:any;
  joiningLocation:any;
 

  nameregx = /^[a-zA-Z]+$/;
  submitData() {
    // //console.log(this.gradDetails.value)
  }
  addskill() {

    //console.log(this.newSkill)

    if (this.newSkill != undefined && this.newSkill.skillName != '') {
      if (!this.skills.includes(this.newSkill))
        this.skills.push(this.newSkill);
    }
  }
  removeskill(skill) {
    //console.log(skill)
    this.skills.splice(skill, 1)
  }
  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must  * Enter a value';
    }
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
  checkPersonalInputs() {
    //console.log(this.zipcode.value.toString().length)
    //console.log(this.contact.value < 1000000000)
    if (this.gradFirstName.hasError('required') || this.gradFirstName.hasError('maxlength') || !this.nameregx.test(this.gradFirstName.value)) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid First Name";
    }
    else if (this.gradMiddleName.hasError('required') || this.gradMiddleName.hasError('maxlength') || !this.nameregx.test(this.gradMiddleName.value)) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid Middle Name";
    }
    else if (this.email.hasError('required') || this.email.hasError('email') || this.email.hasError('maxlength')) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid Email";
    }
    else if (this.contact.hasError('required') || this.contact.value.toString().length != 10) {
      this.inputError = true;
      this.errorMsg = " * Enter 10 digit contact number";
    }
    else if (this.gradLastName.hasError('required') || this.gradLastName.hasError('maxlength') || !this.nameregx.test(this.gradLastName.value)) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid Last Name";
    }
    else if (this.houseNo.hasError('required') || this.houseNo.hasError('maxlength')) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid House No";
    }
    else if (this.street.hasError('required') || this.street.hasError('maxlength')) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid Street Name";
    }
    else if (this.city.hasError('required') || this.city.hasError('maxlength') || !this.nameregx.test(this.city.value)) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid City";
    }
    else if (this.state.hasError('required') || this.state.hasError('maxlength') || !this.nameregx.test(this.state.value)) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid state";
    }

    else if (this.contact.hasError('required') || this.zipcode.value.toString().length != 6) {
      this.inputError = true;
      this.errorMsg = " * Enter avlid zipcode";
    }
    else {
      this.inputError = false
    }
  }
  checkEducationalDeatils() {
    if (this.institute.hasError('required') || this.institute.hasError('maxlength')) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid Institute Name";
    }
    else if (this.degree.hasError('required') || this.degree.hasError('maxlength')) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid Qualification";
    }
    else if (this.branch.hasError('required') || this.branch.hasError('maxlength')) {
      this.inputError = true;
      this.errorMsg = " * Enter Valid Branch";
    }
    else {
      this.inputError = false;
    }
  }
  checkProfessionalDetails() {
    if (this.dateOfJoining.hasError('required')) {
      //console.log("kkk")
      this.inputError = true;
      this.errorMsg = " * Enter Joining Date";
    }
    else if (this.joiningLocation.hasError('required') || this.joiningLocation.hasError('maxlength')) {
      //console.log("llll")
      this.inputError = true;
      this.errorMsg = " * Enter Valid Joining Location";
    }
    else {
      //console.log("mmm")
      this.inputError = false;
    }
  }
  proceedToED() {
    //console.log("pp")
    this.inputError = false
    this.checkPersonalInputs();
    if (!this.inputError) {
      this.educationalDetails = false;
      this.personalDetails = true;
      this.professionalDetails = true;
      this.submitform = true;
      this.progress = 33;
      this.inputError = false;
      this.errorMsg = undefined;
    }
  }
  proceedToPD() {

    this.inputError = false
    this.checkEducationalDeatils();
    if (!this.inputError) {
      this.professionalDetails = false;
      this.educationalDetails = true;
      this.personalDetails = true;
      this.submitform = true;
      this.progress = 66
    }
    ////console.log(this.professionalDetails)
  }
  proceedtoPSD() {
    this.professionalDetails = true;
    this.educationalDetails = true;
    this.personalDetails = false;
    this.submitform = true;
    this.progress = 0
  }
  proceedtoSubmit() {
    this.inputError = false;

    this.checkProfessionalDetails();
    //console.log(this.inputError)
    if (!this.inputError) {
      this.educationalDetails = true;
      this.professionalDetails = true;
      this.personalDetails = true;
      this.submitform = false;
      this.progress = 100
    }
  }
  submitForm() {
    let grad = new GradDetails(localStorage.getItem('user'),this.data.createdOn , localStorage.getItem('user'), undefined, this.data.gradId, this.gradFirstName.value,
      this.gradMiddleName.value, this.gradLastName.value, this.gender.value, this.email.value, this.contact.value, this.houseNo.value,
      this.street.value, this.city.value, this.state.value, this.zipcode.value, this.institute.value, this.degree.value, this.branch.value,
      this.dateOfJoining.value, this.joiningLocation.value, '', this.skills);
      //console.log(grad);
      this.gradService.updateGrad(grad);
    this.gradService.updateGrad(grad).subscribe((data)=>{
      //console.log(data);
    });
    this.router.navigate(['/candidate-management/viewGrad']);
  }
  cancel(){
    this.router.navigate(['/candidate-management/viewGrad']);
  }
  editSkills(){
    this.router.navigate(['/candidate-management/editSkills',this.id],{state: {data:this.data}});
  }
}
