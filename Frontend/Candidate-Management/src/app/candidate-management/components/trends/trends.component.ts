import { Component, OnInit } from '@angular/core';
import {TrendsService} from '../../providers/trends.service';
import {trendBarData} from '../../models/trendBarData';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label , SingleDataSet , monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

export class LocationTrend{
  label:string;
  data:[number];
  constructor(label,data)
  {
    this.label=label;
    this.data=data;
  }
}
@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.css']
})
export class TrendsComponent implements OnInit {

locationBar:boolean=false;
skillBar:boolean=false; 
data:any=[];
label:any=[];
option:String="Location and year Trend"
public barChartOptions: ChartOptions = {
  responsive: true,
};
public barChartLabels: Label[] ;
public barChartType: ChartType = 'bar';
public barChartLegend = true;
public barChartPlugins = [];
public barChartData: ChartDataSets[];

pieData:boolean=false;
public pieChartOptions: ChartOptions = {
  responsive: true,
  legend: {  
    display: true  ,

  },
  tooltips: {
    borderWidth: 10,
    borderColor: 'green',
  },

};
public pieChartLabels: Label[] ;
public pieChartData: SingleDataSet = [];
public pieChartType: ChartType = 'pie';
public pieChartLegend = true;
public pieChartPlugins = [];

public skillbarChartOptions: ChartOptions = {
  responsive: true,
  legend: {  
    display: false  ,
  },
};
public skillbarChartLabels: Label[] ;
public skillbarChartType: ChartType = 'bar';
public skillbarChartLegend = true;
public skillbarChartPlugins = [];
public skillbarChartData: ChartDataSets[];

public skillpieChartOptions: ChartOptions = {
  responsive: true,
  legend: {  
    display: true  
  },
  tooltips: {
    borderWidth: 10,
    borderColor: 'green',
  }
};
public skillpieChartLabels: Label[] ;
public skillpieChartData: SingleDataSet = [];
public skillpieChartType: ChartType = 'pie';
public skillpieChartLegend = true;
public skillpieChartPlugins = [];

constructor(private trendsService:TrendsService) {
  
}



  ngOnInit() {
    this.trendsService.getTrends().subscribe((res)=>{
   let data:any;
      //console.log(data);
      data=res;
      for(let i=0;i<data.length;i++ )
      {
        
        let locationData=[];
       
        for(let j=0;j<data[i].yearMap.length;j++)
        {
          if(i===0){
            //console.log("year")
            this.label.push(data[i].yearMap[j].year);
          }
          

          locationData.push(data[i].yearMap[j].count);
        }
        let locationTrend=new LocationTrend(data[i].location,locationData);
        this.data.push(locationTrend);
      }
    this.locationBar=true;
     this.setBarchart();
     this.setPiechart();
    });


   this.trendsService.getSkillTrends().subscribe((res)=>{
    // //console.log("skill",data);
    let data:any=res;

     let pieLabels:Label=[]
     let chartCount:SingleDataSet=[];
     let barcharcount=[];
     let temp=[];
    let  labb=' ';
     for(let i = 0;i<data.length;i++)
     {
       
      pieLabels.push(data[i].skill)
       chartCount.push(data[i].count);
       barcharcount.push(data[i].count);
     }
     //console.log(pieLabels,chartCount);
     this.skillpieChartData=chartCount;
     this.skillpieChartLabels=pieLabels;
     this.skillbarChartLabels=pieLabels;
     //console.log( this.skillbarChartLabels,this.skillpieChartLabels,this.skillpieChartData);
     let x: ChartDataSets={
      data:[] = barcharcount,
    };
    temp.push(x);
    this.skillbarChartData=temp
   })
  }
  setBarchart()
  {
    let tepm=[]; 
    let labels;
    //console.log(this.label)
    for(let i = 0;i<this.data.length;i++)
    {
      let x: ChartDataSets={
        data :[]= this.data[i].data,
        label:String=this.data[i].label
      };
      tepm.push(x)
    }
    this.barChartData=tepm
    this.barChartLabels=this.label;
   //console.log("bar",this.barChartData,this.barChartLabels)
  }
 
  setPiechart()
  {
    let pieLabels:Label=[]
    let piechartCount:SingleDataSet=[];
    for(let i = 0;i<this.data.length;i++)
    {
      
     pieLabels.push(this.data[i].label)
      let count:number=0;
     
      for(let j=0;j<this.data[i].data.length;j++)
      {
        count=this.data[i].data[j]+count;
        
       
      }
      piechartCount.push(count);
    }
    //console.log(pieLabels,piechartCount)
    this.pieChartData=piechartCount;
    this.pieChartLabels=pieLabels;
  }
    locationYearTrend(){
      this.locationBar=true;
      this.pieData=false;
      this.skillBar=false;
      this.option="Location and Year Trend"
    }
    locationTrend(){
      this.locationBar=false;
      this.pieData=true;
      this.skillBar=false;
      this.option="Location Trend"
    }
    skillTrend()
    {
      this.locationBar=false;
      this.pieData=false;
      this.skillBar=true;
      this.option="Skills Trend"
    }
}
