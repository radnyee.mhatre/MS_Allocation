import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewGradComponent } from './components/view-grad/view-grad.component';
import { AddGradComponent } from './components//add-grad/add-grad.component';
import { EditGradComponent } from './components/edit-grad/edit-grad.component';
import { SkillComponent } from './components/skill/skill.component';
import { EditSkillsComponent } from './components/edit-skills/edit-skills.component';
import { TrendsComponent } from './components/trends/trends.component';
import { DisplayDetailsComponent } from './components/display-details/display-details.component'
import { CandidateManagementRoutingModule } from './candidate-management-routing.module';
import { CandidateManagementComponent } from './candidate-management.component';
import { CandidateNavComponent } from './components/candidate-nav/candidate-nav.component';


import { MatFormFieldModule } from '@angular/material/form-field';
import {
  MatInputModule, MatDatepickerModule, MatNativeDateModule,
  MatButtonModule, MatIconModule, MatProgressBarModule, MatRadioModule,
  MatTableModule, MatPaginatorModule, MatSortModule, MatGridListModule
} from '@angular/material';
import { MatMenuModule} from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { ChartsModule } from 'ng2-charts';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GradHistoryComponent } from './components/grad-history/grad-history.component';

@NgModule({
  declarations: [CandidateManagementComponent,

    ViewGradComponent,
    AddGradComponent,
    EditGradComponent,
    AddGradComponent,

    SkillComponent,
    DisplayDetailsComponent,
    EditSkillsComponent,
    TrendsComponent,
    CandidateNavComponent,
    GradHistoryComponent,


  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    CandidateManagementRoutingModule,
    MatInputModule, MatDatepickerModule, MatNativeDateModule,
    MatButtonModule, MatIconModule, MatProgressBarModule, MatRadioModule,
    MatTableModule, MatPaginatorModule, MatSortModule, MatGridListModule,
    MatSelectModule, ChartsModule,MatMenuModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class CandidateManagementModule { }
