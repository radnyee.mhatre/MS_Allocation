import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DeteteGradService {

  constructor(private http:HttpClient) { }
  deleteGrad(id:number): Observable<any>
  {
    return this.http.get('/api/deleteGrad/'+id);
  }
}
