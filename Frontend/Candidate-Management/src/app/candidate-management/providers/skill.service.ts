import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {skill} from '../models/skills'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SkillService {

  constructor(private http: HttpClient) { }
  addSkill(skillName): Observable<any>
  {
    console.log(localStorage.user);
    let sk=new skill(-1,skillName,'',localStorage.getItem('user'));
    console.log(sk);
    return this.http.post('/api/addSkill',sk);
  }

  getSkills(): Observable<any>
  {
    return this.http.get('/api/skills');
  }
  updateSkills(Id,Skills): Observable<any>
  {
    console.log("service",Skills)
    return this.http.post('/api/addSkills/'+Id,Skills);
  }
}
