import { TestBed } from '@angular/core/testing';

import { DeteteGradService } from './detete-grad.service';

describe('DeteteGradService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeteteGradService = TestBed.get(DeteteGradService);
    expect(service).toBeTruthy();
  });
});
