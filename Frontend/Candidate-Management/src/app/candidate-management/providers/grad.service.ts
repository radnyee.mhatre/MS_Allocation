import { Injectable } from '@angular/core';
import {GradDetails} from '../models/GradDetails';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class GradService {

  constructor(private http: HttpClient) { }
  addGrad(grad:GradDetails): Observable<any>
  {
  //  console.log(grad)
    return this.http.post('/api/addGrad',grad);
  }
  getGrads(): Observable<any>
  {
    return this.http.get('/api/grad');
  }
  getGrad(id:number): Observable<any>
  {
    return this.http.get('/api/grad/'+id);
  }
  updateGrad(grad:GradDetails): Observable<any>
  {
    return this.http.post('/api/updateGrad',grad);
  }
  getHistory(gradId:number): Observable<any>
  {
    console.log(gradId);
    return this.http.get('/api/history/'+gradId);
  }
}
