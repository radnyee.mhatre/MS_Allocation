import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TrendsService {

  constructor(private http: HttpClient) { }
  getTrends(): Observable<any>
  {
    return this.http.get('/api/trends');
  }
  getSkillTrends(): Observable<any>
  {
    return this.http.get('/api/skillTrend');
  }
}
