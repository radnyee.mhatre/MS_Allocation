import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {AuthGaurdService as AuthGuard} from './providers/auth-gaurd.service';


const routes: Routes = [
  {path : '', component : LoginComponent },
  {path : 'login', component : LoginComponent },
  {path : 'home', component : HomeComponent,canActivate: [AuthGuard] },
   { path: 'candidate-management',
   loadChildren: () => import('./candidate-management/candidate-management.module').then(m => m.CandidateManagementModule) },
  {path : '**', component : NotFoundComponent ,canActivate: [AuthGuard] },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
