export class LoginUser {  
    email: string;  
    token: string;  
    name:string;
    id_token :string
    constructor(email,name,token,id_token){
        this.email=email;
        this.token=token;
        this.name=name;
        this.id_token=id_token;
    }
    
}