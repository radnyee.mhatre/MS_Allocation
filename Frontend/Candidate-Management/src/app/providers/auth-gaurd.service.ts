import { Injectable } from '@angular/core';
import { Router } from '@angular/router'; 

@Injectable({
  providedIn: 'root'
})
export class AuthGaurdService {

  constructor(private router: Router)  { }

  isAuthenticated(){
    return !!localStorage.getItem('user');
  }
 
    canActivate(): boolean {
      if (!this.isAuthenticated()) {
        this.router.navigate(['login']);
        return false;
      }
      return true;
  
  }
}
