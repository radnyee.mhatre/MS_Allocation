import { Component, OnInit } from '@angular/core';
import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';  
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';  
import { Socialusers } from '../../models/SocialUser';  
import { SocialloginService } from '../../providers/sociallogin.service';  
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  response;  
  socialusers=new Socialusers();  
constructor(  
  public OAuth: AuthService,  
  private SocialloginService: SocialloginService,  
  private router:Router,
  
) { }  
ngOnInit() {  
}  
public socialSignIn(socialProvider: string) {  
  let socialPlatformProvider;  
  if (socialProvider === 'facebook') {  
    socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;  
  } else if (socialProvider === 'google') {  
    socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;  
  }  
  this.OAuth.signIn(socialPlatformProvider).then(socialusers => {  

    this.savesResponse(socialusers);  
  });  
}  
savesResponse(socialusers: Socialusers) {
  localStorage.setItem('idToken',socialusers.idToken);  
  this.SocialloginService.Savesresponse(socialusers).subscribe((res: any) => {  
    debugger;    
    localStorage.setItem('user', res.email);   
   this.router.navigate(['home']);
 })  
 
}  
}
