package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;

import com.backend.msallocation.model.LocationTrend;
import com.backend.msallocation.model.SkillMap;
import com.backend.msallocation.model.YearMap;
import com.backend.msallocation.service.TrendsServiceImpl;
import com.backend.msallocation.validator.TokenValidator;
@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
public class TrendsControllerTest {
	@Mock 
	TokenValidator validator;
	
	@Mock
	TrendsServiceImpl trendService;

	@InjectMocks
	TrendsControllerImpl trendController;
	
	@Test
	public void testTrendsController() throws GeneralSecurityException, IOException{
		 Mockito.when(validator.validateToken("abc")).thenReturn(true);
		
		SkillMap skillMapJava=new SkillMap("Java",10);
		SkillMap skillMapAngular=new SkillMap("Angular",12);
		SkillMap skillMapReact=new SkillMap("React",14);
		SkillMap skillMapSpring=new SkillMap("Spring",8);
		
		List<SkillMap> skillTrend = new LinkedList<SkillMap>();
		skillTrend.add(skillMapAngular);
		skillTrend.add(skillMapJava);
		skillTrend.add(skillMapReact);
		skillTrend.add(skillMapSpring);
		
		String year[]= new String[]{"2018","2019","2020"};
		String location[]= new String[]{"Mumbai","Bangalore","Hyderabad"};
		int count[]= new int[]{10,12,15};
		
		List<YearMap> yeartrend=new LinkedList<YearMap>();
		List<LocationTrend> locatioTrend=new LinkedList<LocationTrend>();
		for(int i=0;i<3;i++)
		{
			yeartrend.add(new YearMap(year[i],count[i]));
		}
		for(int i=0;i<3;i++)
		{
			locatioTrend.add(new LocationTrend(count[i],location[i],yeartrend));
		}
		Mockito.when(trendService.getTrends()).thenReturn(locatioTrend);
		Mockito.when(trendService.getSkilTrend()).thenReturn(skillTrend);
		
		Assert.assertEquals(locatioTrend.size(),((List<SkillMap>) trendController.getTrends("abc").getBody()).size());
		Assert.assertEquals(HttpStatus.UNAUTHORIZED, trendController.getTrends("asbc").getStatusCode());
		
		Assert.assertEquals(skillTrend.size(), ((List<SkillMap>) trendController.getSkillsTrends("abc").getBody()).size());
		Assert.assertEquals(HttpStatus.UNAUTHORIZED, trendController.getSkillsTrends("asbc").getStatusCode());
	}
}
