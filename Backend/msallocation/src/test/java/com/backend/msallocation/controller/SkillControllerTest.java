package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;

import com.backend.msallocation.model.Skill;
import com.backend.msallocation.service.SkillServiceImpl;
import com.backend.msallocation.validator.TokenValidator;


@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
public class SkillControllerTest {
	@Mock
	TokenValidator validator;
	
	@Mock
	SkillServiceImpl SkillService;
	
	@InjectMocks
	SkillControllerImpl skillController;
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void addSkill() throws GeneralSecurityException, IOException {
		 Mockito.when(validator.validateToken("abc")).thenReturn(true);
		
		String createdBy="radnyee01@gmail.com";
		Date dNow = new Date( );
	      SimpleDateFormat ft = 
	      new SimpleDateFormat ("yyyy-MM-dd");
		Skill newSkill= new Skill(createdBy,dNow,10,"Java");
		List<Skill> skillList=new LinkedList<Skill>();
		for(int i=0;i<8;i++)
		{
			skillList.add(new Skill(createdBy,dNow,i,"Java"));
		}
		skillList.add(newSkill);
		 Mockito.when(SkillService.addSkill(newSkill)).thenReturn(skillList);
		 Mockito.when(SkillService.getSkills()).thenReturn(skillList);
		 
		Assert.assertEquals(true,((List<Skill>) skillController.addSkill("abc", newSkill).getBody()).contains(newSkill) );
		Assert.assertEquals(HttpStatus.UNAUTHORIZED,skillController.addSkill("cde", newSkill).getStatusCode() );
		
		Assert.assertEquals(9,((List<Skill>) skillController.getSkills("abc").getBody()).size());
		Assert.assertEquals(HttpStatus.UNAUTHORIZED,skillController.getSkills("cde").getStatusCode() );
		
	}

}
