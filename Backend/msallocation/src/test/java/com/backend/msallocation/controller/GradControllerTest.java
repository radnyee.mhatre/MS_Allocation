package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;

import com.backend.msallocation.model.Grad;
import com.backend.msallocation.model.Skill;
import com.backend.msallocation.service.GradServiceImpl;
import com.backend.msallocation.validator.TokenValidator;

@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
public class GradControllerTest {
	@Mock
	TokenValidator validator;
	
	@Mock
	GradServiceImpl gradService;
	
	@InjectMocks
	GradControllerImpl gradController;
	
	
	
	@Test
	public void TestGrad() throws GeneralSecurityException, IOException {
		
		 Mockito.when(validator.validateToken("abc")).thenReturn(true);
		 
		 String createdBy="radnyee01@gmail.com";
			Date dNow = new Date( );
		      SimpleDateFormat ft = 
		      new SimpleDateFormat ("yyyy-MM-dd");

			Skill newSkill= new Skill(createdBy,dNow,10,"Java");
	List<Skill> skillList=new LinkedList<Skill>();
	for(int i=0;i<8;i++)
	{
		skillList.add(new Skill(createdBy,dNow,i,"Java"));
	}
	
	Grad grad=new Grad(createdBy, dNow , 5,  "a",  "b", "c",  "male",  "email@emailo.com",  1111256987
			,"aa", "street",  "city", "state",  "112568" ,  "institute", " degree", " branch",  dNow,
			 "mumbai",  "feedback", skillList);
	List <Grad> gradList=new LinkedList<Grad>();
	for(int i=0;i<4;i++)
	{
		gradList.add(
				new Grad(createdBy, dNow , i,  "a",  "b", "c",  "male",  "email@emailo.com",  1111256987
						,"aa", "street",  "city", "state",  "112568" ,  "institute", " degree", " branch",  dNow,
						 "mumbai",  "feedback", skillList)
				);
	}
	gradList.add(grad);
	Grad grad_6=new Grad(createdBy, dNow , 6,  "a",  "b", "c",  "male",  "email@emailo.com",  1111256987
			,"aa", "street",  "city", "state",  "112568" ,  "institute", " degree", " branch",  dNow,
			 "mumbai",  "feedback", skillList);
	 Mockito.when(gradService.addGrad(grad)).thenReturn(grad);
	Mockito.when(gradService.addSkills(5, skillList)).thenReturn(grad);
	Mockito.when(gradService.deleteGrad(6)).thenReturn(gradList);
	Mockito.when(gradService.getGrad(5)).thenReturn(grad);
	Mockito.when(gradService.getGrads()).thenReturn(gradList);
	Mockito.when(gradService.updateGrad(grad)).thenReturn(grad);
	Mockito.when(gradService.getHistory(5)).thenReturn(gradList);
	
	Assert.assertEquals(grad, gradController.addGrad("abc",grad).getBody());
	Assert.assertEquals(HttpStatus.UNAUTHORIZED, gradController.addGrad("cde",grad).getStatusCode());
	
	Assert.assertEquals(grad.getSkills().size(), ((Grad) gradController.addSkills("abc",5,skillList).getBody()).getSkills().size());
	Assert.assertEquals(HttpStatus.UNAUTHORIZED, gradController.addSkills("cde",5,skillList).getStatusCode());
	
	Assert.assertEquals(gradList.size(), ((List<Skill>) gradController.deleteGrad("abc",6).getBody()).size());
	Assert.assertEquals(HttpStatus.UNAUTHORIZED, gradController.deleteGrad("cde",0).getStatusCode());
	
	Assert.assertEquals(grad, gradController.getGrad("abc",5).getBody());
	Assert.assertEquals(HttpStatus.UNAUTHORIZED, gradController.getGrad("cde",5).getStatusCode());
	
	Assert.assertEquals(gradList.size(), ((List<Skill>) gradController.getGrads("abc").getBody()).size());
	Assert.assertEquals(HttpStatus.UNAUTHORIZED, gradController.getGrads("cde").getStatusCode());
	
	Assert.assertEquals(grad.getGradFirstName(), ((Grad) gradController.updateGrad("abc",grad).getBody()).getGradFirstName());
	Assert.assertEquals(HttpStatus.UNAUTHORIZED, gradController.updateGrad("cde",grad).getStatusCode());

	Assert.assertEquals(gradList.size(), ((List<Grad>) gradController.gethistory("abc",5).getBody()).size());
	Assert.assertEquals(HttpStatus.UNAUTHORIZED, gradController.gethistory("cde", 5).getStatusCode());
	
	}

}
