package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;

import com.backend.msallocation.dto.LoginUser;
import com.backend.msallocation.service.LoginService;
import com.backend.msallocation.validator.TokenValidator;


@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {
	
	@Mock
	TokenValidator validator;
	
	@Mock
	LoginService loginService;
	
	@InjectMocks
	LoginControllerImpl loginController;
	
		@Test
		public void checkLoginController() throws GeneralSecurityException, IOException{
		 
		 Mockito.when(validator.validateToken("abc")).thenReturn(true);
		 
		String url="http://localhost:8080/api/login";
		 RestTemplate restTemplate = new RestTemplate();
		    LoginUser loginUser=new LoginUser("radnyee01@gmail.com","aaaassssd","Radnyee Mhatre","abc");
		    Mockito.when(loginService.addUser(loginUser)).thenReturn(loginUser);
		    
		    Assert.assertEquals(loginUser, loginController.addUser("abc", loginUser).getBody());
		    Assert.assertEquals(HttpStatus.UNAUTHORIZED,loginController.addUser("cdw", loginUser).getStatusCode());
		    
	}
		

}
