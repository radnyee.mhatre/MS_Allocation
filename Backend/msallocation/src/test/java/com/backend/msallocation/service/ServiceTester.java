package com.backend.msallocation.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;

import com.backend.msallocation.dto.LoginUser;
import com.backend.msallocation.model.Grad;
import com.backend.msallocation.model.Skill;
import com.google.api.services.calendar.Calendar;


@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
public class ServiceTester {
	
	@Autowired
	GradService gradService;
	
	@Autowired 
	SkillService skillService;
	
	@Autowired
	TrendsService trendsService;
	
	@Autowired 
	LoginService loginService;
	
	@Test
	public void testTrendService()
	{ 
		Random r= new Random();
		 LoginUser loginUser=new LoginUser("radnyee01@gmail.com","aaaassssd","Radnyee Mhatre","abc");
		String createdBy="radnyee01@gmail.com";
		Date dNow = new Date();
	      SimpleDateFormat ft = 
	      new SimpleDateFormat ("yyyy-MM-dd");

	      List<Skill> skillList=skillService.getSkills();
		Skill newSkill= new Skill(createdBy,dNow,10,"Java");
		Assert.assertEquals(skillList.size()+1, skillService.addSkill(newSkill).size());
		skillList=skillService.getSkills();
		
		Grad grad=new Grad(createdBy, dNow , 5,  "a",  "b", "c",  "male",  "email@emailo.com",  1111256987
				,"aa", "street",  "city", "state",  "112568" ,  "institute", " degree", " branch",  dNow,
				 "mumbai",  "feedback", skillList);
		List<Grad> gradList=gradService.getGrads();
		Assert.assertEquals(grad.getEmail(), gradService.addGrad(grad).getEmail());
		
		gradList=gradService.getGrads();
		Assert.assertEquals(gradList.get(gradList.size()-1).getGradId(),gradService.addSkills(gradList.get(gradList.size()-1).getGradId(), skillList).getGradId());
		
		
		Assert.assertEquals(true, (gradService.getGrad(gradList.get(gradList.size()-1).getGradId())).getGradId()>0);
		Assert.assertEquals(gradList.get(gradList.size()-1).getGradFirstName(), gradService.updateGrad(gradList.get(gradList.size()-1)).getGradFirstName());
		Assert.assertEquals(gradList.size()-1, gradService.deleteGrad(gradList.get(gradList.size()-1).getGradId()).size());
		System.out.println( "--------------"+gradService.getHistory(5));
		
		Assert.assertEquals(true,trendsService.getTrends().size()>=0);
		 Assert.assertEquals(true,trendsService.getSkilTrend().size()>=0);
		 
		 
		 Assert.assertEquals(loginUser.getEmail(), loginService.addUser(loginUser).getEmail());
		 String Email=loginService.addUser(loginUser).getEmail()+Integer.toString(r.nextInt());
		 
		 
		 Assert.assertEquals(Email, loginService.addUser(new LoginUser(Email,"aaaassssd","Radnyee Mhatre","abc")).getEmail());
	 
	}
}
