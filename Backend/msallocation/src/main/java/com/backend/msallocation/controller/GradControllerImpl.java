package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.msallocation.model.Grad;
import com.backend.msallocation.model.Skill;
import com.backend.msallocation.service.GradService;
import com.backend.msallocation.service.SkillService;
import com.backend.msallocation.validator.TokenValidator;

@RestController
@RequestMapping("/api")
public class GradControllerImpl implements GradController {
	Logger logger = LoggerFactory.getLogger(GradControllerImpl.class);
	@Autowired
	GradService gradService;
	TokenValidator validator=new TokenValidator();
	
	@PostMapping("/addGrad")
	@Override
	public ResponseEntity<?> addGrad(@RequestHeader("idToken")  String idToken, @RequestBody Grad grad) throws GeneralSecurityException, IOException {
		logger.info("Grad Controller Validating login token :"+idToken);
		if(validator.validateToken(idToken)) {
			logger.info("Grad Controller Valid login token :"+idToken);
		logger.info("Add grad controller for grad:"+grad.getGradFirstName()+" "+grad.getGradLastName());
		return new ResponseEntity<>(gradService.addGrad(grad), HttpStatus.OK); 
		}
		else 
		{
			logger.info("Grad Controller invalid token :"+idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);		}
	}

	@PostMapping("/updateGrad")
	@Override
	public ResponseEntity<?> updateGrad(@RequestHeader("idToken")String idToken, @RequestBody Grad grad) throws GeneralSecurityException, IOException {
		logger.info("Grad Controller Validating login token :"+idToken);
		if(validator.validateToken(idToken)) {
			logger.info("Grad Controller Valid login token :"+idToken);
		logger.info("Update grad controller for grad id:"+grad.getGradId());
		// TODO Auto-generated method stub
		return new ResponseEntity<>(gradService.updateGrad(grad), HttpStatus.OK); 
		}
		else 
		{
			logger.info("Grad Controller invalid token :"+idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);		}
	}

	@GetMapping("/deleteGrad/{gradId}")
	@Override
	public ResponseEntity<?> deleteGrad(@RequestHeader("idToken")String idToken, @PathVariable("gradId") Integer gradId) throws GeneralSecurityException, IOException {
		if(validator.validateToken(idToken)) {
		logger.info("Grad Controller Valid login token :"+idToken);
	logger.info("Update grad controller for grad id:"+gradId);
	// TODO Auto-generated method stub
	return new ResponseEntity<>(gradService.deleteGrad(gradId), HttpStatus.OK); 
	}
	else {
		
		return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);	}
	}

	@GetMapping("/grad/{gradId}")
	@Override
	public ResponseEntity<?> getGrad(@RequestHeader("idToken") String idToken, @PathVariable("gradId") Integer gradId) throws GeneralSecurityException, IOException {
		logger.info("Get grad controller for grad id:"+gradId);
		logger.info("Grad Controller Validating login token :"+idToken);
		if(validator.validateToken(idToken)) {
			logger.info("Grad Controller Valid login token :"+idToken);
		return new ResponseEntity<>(gradService.getGrad(gradId), HttpStatus.OK); 
		
		}
		else 
		{
			logger.info("Grad Controller invalid token :"+idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);		}
	}

	@GetMapping("/grad")
	@Override
	public ResponseEntity<?>  getGrads(@RequestHeader("idToken") String idToken) throws GeneralSecurityException, IOException {
		// TODO Auto-generated method stub
		logger.info("Get all grad controller");
		logger.info("Grad Controller Validating login token :"+idToken);
		if(validator.validateToken(idToken)) {
			logger.info("Grad Controller Valid login token :"+idToken);
			return new ResponseEntity<>( gradService.getGrads(), HttpStatus.OK);
		}
		else 
		{
			logger.info("Grad Controller invalid token :"+idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);		}
	}

	@PostMapping("/addSkills/{gradId}")
	@Override
	public ResponseEntity<?> addSkills(@RequestHeader("idToken")String idToken, @PathVariable("gradId") Integer gradId,  @RequestBody List<Skill> skills) throws GeneralSecurityException, IOException {
		logger.info("Grad Controller Validating login token :"+idToken);
		if(validator.validateToken(idToken)) {
			logger.info("Grad Controller Valid login token :"+idToken);
			return new ResponseEntity<>( gradService.addSkills(gradId, skills), HttpStatus.OK);
		}
		else 
		{
			logger.info("Grad Controller invalid token :"+idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);		}
	}

	@GetMapping("/history/{id}")
	@Override
	public ResponseEntity<?> gethistory(@RequestHeader("idToken") String idToken, @PathVariable("id") int gradId) throws GeneralSecurityException, IOException {
		logger.info("Get History controller");
		logger.info("Grad Controller Validating login token :"+idToken);
		if(validator.validateToken(idToken)) {
			logger.info("Grad Controller Valid login token :"+idToken);
			return new ResponseEntity<>( gradService.getHistory(gradId), HttpStatus.OK);
		}
		else 
		{
			logger.info("Grad Controller invalid token :"+idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);		}
	}
}
