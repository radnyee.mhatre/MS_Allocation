package com.backend.msallocation.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.backend.msallocation.constants.Queries;
import com.backend.msallocation.controller.GradControllerImpl;
import com.backend.msallocation.model.Grad;
import com.backend.msallocation.rowMapper.GradRowMapper;

@Repository
public class GradRepository {
	Logger logger = LoggerFactory.getLogger(GradRepository.class);
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	public int addGrad(Grad grad)
	{
		logger.info("Grad Repository adding grad"+grad.getGradFirstName()+" "+grad.getGradLastName());
		Date date = Calendar.getInstance().getTime();
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("fname", grad.getGradFirstName())
				.addValue("mname", grad.getGradMiddleName()).addValue("lname", grad.getGradLastName()).addValue("gender", grad.getGender())
				.addValue("email", grad.getEmail()).addValue("contact", grad.getContact()).addValue("houseNo", grad.getHouseNo())
				.addValue("street", grad.getStreet())
				.addValue("city", grad.getCity())
				.addValue("state", grad.getState())
				.addValue("zipcode", grad.getZipcode())
				.addValue("institute", grad.getInstitute())
				.addValue("degree", grad.getDegree())
				.addValue("branch", grad.getBranch())
				.addValue("doj", grad.getJoiningDate())
				.addValue("location", grad.getJoiningLocation())
				.addValue("createdBy", grad.getCreatedBy())
				.addValue("createdOn", date);
		jdbcTemplate.update(Queries.INSERT_GRAD, parameters);
		List <Integer> id = jdbcTemplate.query("select last_insert_id() from grad_details;", 
		         new ResultSetExtractor<List<Integer>>(){
		         
		         public List<Integer> extractData(
		            ResultSet rs) throws SQLException, DataAccessException {
		            
		            List<Integer> list = new ArrayList<Integer>();  
		            while(rs.next()){  
		               list.add(rs.getInt("last_insert_id()"));  
		            }  
		            return list;  
		         }    	  
		      });
		return id.get(0);
	}
	
	public void addVersion(Grad grad) {
		logger.info("Grad Repository adding version for grad id"+grad.getGradId());
		Date date = Calendar.getInstance().getTime();
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("fname", grad.getGradFirstName())
				.addValue("mname", grad.getGradMiddleName()).addValue("lname", grad.getGradLastName()).addValue("gender", grad.getGender())
				.addValue("email", grad.getEmail()).addValue("contact", grad.getContact()).addValue("houseNo", grad.getHouseNo())
				.addValue("street", grad.getStreet())
				.addValue("city", grad.getCity())
				.addValue("state", grad.getState())
				.addValue("zipcode", grad.getZipcode())
				.addValue("institute", grad.getInstitute())
				.addValue("degree", grad.getDegree())
				.addValue("branch", grad.getBranch())
				.addValue("doj", grad.getJoiningDate())
				.addValue("location", grad.getJoiningLocation())
				.addValue("createdBy", grad.getCreatedBy())
				.addValue("createdOn", grad.getCreatedOn())
				.addValue("parentId", grad.getGradId());
		jdbcTemplate.update(Queries.INSERT_GRAD_VERSION, parameters);
	}
	public void updateGrad(Grad grad)
	{
		logger.info("Grad Repository updating grad"+grad.getGradId());
		Date date = Calendar.getInstance().getTime();
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("gradId", grad.getGradId())
				.addValue("fname", grad.getGradFirstName())
				.addValue("mname", grad.getGradMiddleName())
				.addValue("lname", grad.getGradLastName())
				.addValue("gender", grad.getGender())
				.addValue("email", grad.getEmail())
				.addValue("contact", grad.getContact())
				.addValue("houseNo", grad.getHouseNo())
				.addValue("street", grad.getStreet())
				.addValue("city", grad.getCity())
				.addValue("state", grad.getState())
				.addValue("zipcode", grad.getZipcode())
				.addValue("institute", grad.getInstitute())
				.addValue("degree", grad.getDegree())
				.addValue("branch", grad.getBranch())
				.addValue("doj", grad.getJoiningDate())
				.addValue("location", grad.getJoiningLocation())
				.addValue("createdBy", grad.getCreatedBy())
				.addValue("createdOn", date)
				.addValue("feedback", grad.getFeedback());	
		jdbcTemplate.update(Queries.UPDATE_GRAD, parameters);
	}
	
	public List<Grad> getGrads(int gradId)
	{
		logger.info("Grad Repository Getting grad details for grad id"+gradId);
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("gradId", gradId);
		return jdbcTemplate.query(Queries.GRAD_BY_ID, parameters, GradRowMapper.gradRowMapperLambda);
	}
	public List<Grad> getGrads()
	{
		logger.info("Grad Repository get all grads");
		return jdbcTemplate.query(Queries.ALL_GRADS, GradRowMapper.gradRowMapperLambda);
	}
	public Grad getGrad(int gradId)
	{
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("gradId", gradId);
		return jdbcTemplate.query(Queries.GRAD_BY_ID, parameters, GradRowMapper.gradRowMapperLambda).get(0);
	}
	public void deletegrad(int gradId)
	{
		logger.info("Grad Repository deleting grad"+gradId);
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("gradId", gradId);
		jdbcTemplate.update(Queries.DELETE_FROM_GRAD, parameters);
	}

	public List<Grad> getHistory(int gradId) {
		logger.info("Grad Repository getting History for gradId: "+gradId);
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("gradId", gradId);
		return jdbcTemplate.query(Queries.GET_HISTORY, parameters,GradRowMapper.gradRowMapperLambda);
	}
}
