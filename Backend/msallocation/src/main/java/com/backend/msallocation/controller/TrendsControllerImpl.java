package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.msallocation.service.TrendsService;
import com.backend.msallocation.validator.TokenValidator;

@RestController
@RequestMapping("/api")
public class TrendsControllerImpl implements TrendsController {
	Logger logger = LoggerFactory.getLogger(TrendsControllerImpl.class);
	@Autowired
	TrendsService trendsService;
	TokenValidator validator = new TokenValidator();

	@RequestMapping("/trends")
	@Override
	public ResponseEntity<?> getTrends(@RequestHeader("idToken") String idToken)
			throws GeneralSecurityException, IOException {
		logger.info("Tred Controller Validating login token :" + idToken);
		if (validator.validateToken(idToken)) {
			logger.info("Trend Controller Valid login token :" + idToken);
			logger.info("Controller Get Trends");
			return new ResponseEntity<>(trendsService.getTrends(), HttpStatus.OK);
		} 
		else {
			logger.info("Trend Controller invalid token :" + idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);		}
	}

	@GetMapping("/skillTrend")
	@Override
	public ResponseEntity<?> getSkillsTrends(@RequestHeader("idToken") String idToken) throws GeneralSecurityException, IOException {
		logger.info("Tred Controller Validating login token :" + idToken);
		if (validator.validateToken(idToken)) {
			logger.info("Trend Controller Valid login token :" + idToken);
			logger.info("Controller Get Trends");
			return new ResponseEntity<>(trendsService.getSkilTrend(), HttpStatus.OK);

		} else {
			logger.info("Trend Controller invalid token :" + idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);		
			}
	}

}
