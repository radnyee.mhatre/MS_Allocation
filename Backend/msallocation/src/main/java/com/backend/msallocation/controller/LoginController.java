package com.backend.msallocation.controller;


import java.io.IOException;
import java.security.GeneralSecurityException;

import org.springframework.http.ResponseEntity;

import com.backend.msallocation.dto.LoginUser;


public interface LoginController {
	public ResponseEntity<?> addUser(String idToken,LoginUser loginuser) throws GeneralSecurityException, IOException;

}
