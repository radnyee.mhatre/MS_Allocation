package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.springframework.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.msallocation.dto.LoginUser;
import com.backend.msallocation.service.LoginService;
import com.backend.msallocation.validator.TokenValidator;



@RestController
@RequestMapping("/api")
public class LoginControllerImpl implements LoginController {
	Logger logger = LoggerFactory.getLogger(LoginControllerImpl.class);
	TokenValidator validator=new TokenValidator();
	@Autowired
	LoginService loginService;
	@PostMapping("/login")
	public ResponseEntity<?> addUser(@RequestHeader("idToken") String idToken,@RequestBody LoginUser loginUser) throws GeneralSecurityException, IOException {
		logger.info(" Controller Validating login token :"+idToken);
		if(validator.validateToken(idToken)) {
			logger.info("Login Controller Valid login token :"+idToken);
			logger.info("Login Controller Adding user "+loginUser.getEmail());
			return new ResponseEntity<>(loginService.addUser(loginUser), HttpStatus.OK);
		}
			
		else {
			
			logger.warn("Login Controller Invalid Login token :"+idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);
		}
		
	}
	
}
