package com.backend.msallocation.service;

import com.backend.msallocation.dto.LoginUser;

public interface LoginService {

	public LoginUser addUser(LoginUser user);
}
