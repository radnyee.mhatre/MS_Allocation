package com.backend.msallocation.rowMapper;

import org.springframework.jdbc.core.RowMapper;

import com.backend.msallocation.constants.Queries;
import com.backend.msallocation.dto.LoginUser;

public class UserRowMapper {
	public static final RowMapper<LoginUser> loginRowMapperLambda=(rs,rownum)-> {
		LoginUser user=new LoginUser();
		user.setEmail(rs.getString(Queries.USER_EMAIL));
		user.setName(rs.getString(Queries.USER_NAME));
		user.setToken(rs.getString(Queries.USER_TOKEN));
        return user;
	};

}
