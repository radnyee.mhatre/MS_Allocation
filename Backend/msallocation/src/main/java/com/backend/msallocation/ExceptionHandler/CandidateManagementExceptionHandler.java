package com.backend.msallocation.ExceptionHandler;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class CandidateManagementExceptionHandler {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	public ResponseEntity<Error> resourceExceptionHandler(RuntimeException re) {
		if (re instanceof IllegalArgumentException) {
			logger.error("Encountered IllegalArgumentException - returning BAD_REQUEST");
			return new ResponseEntity<>(new Error(re), HttpStatus.BAD_REQUEST);
		} 
		else if(re instanceof NullPointerException) {
		logger.error("Encountered unexpected exception while processing runtime exception" +re);
		return new ResponseEntity<>(new Error(re), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else
		return new ResponseEntity<>(new Error(re), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
