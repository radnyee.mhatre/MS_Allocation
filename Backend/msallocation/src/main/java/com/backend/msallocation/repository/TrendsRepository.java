package com.backend.msallocation.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.backend.msallocation.constants.Queries;

@Repository
public class TrendsRepository {
	Logger logger = LoggerFactory.getLogger(TrendsRepository.class);
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	public List<String> getLocations() {
		logger.info("Trends Repository  getting all unique locations for trends");
		List<String> locations = jdbcTemplate.query(Queries.DISTINCT_LOCATIONS, new ResultSetExtractor<List<String>>() {

			public List<String> extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<String> list = new ArrayList<String>();
				while (rs.next()) {
					list.add(rs.getString(Queries.JOINING_LOCATION));
				}
				return list;
			}
		});
		return locations;
	}

	public int getCount(String startDate, String endDate, String location) {
		logger.info("Trends Repository getting grad count for join in between " + startDate + " and " + endDate
				+ " for location " + location);
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("startDate", startDate)
				.addValue("endDate", endDate).addValue("location", location);
		return jdbcTemplate.queryForObject(Queries.COUNT_GRADS, parameters, Integer.class);

	}
	
	public int getSkillCount(int SkillId)
	{
		logger.info("Trends repository getting count for skill id"+SkillId);
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("skillId", SkillId);
		return jdbcTemplate.queryForObject(Queries.SKILL_COUNTS, parameters, Integer.class);
	}
}
