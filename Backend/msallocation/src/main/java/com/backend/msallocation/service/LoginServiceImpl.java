package com.backend.msallocation.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.msallocation.dto.LoginUser;
import com.backend.msallocation.repository.LoginRepository;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	LoginRepository loginRepository;
	@Override
	public LoginUser addUser(LoginUser user) {
		Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
		// TODO Auto-generated method stub
		if(loginRepository.getUser(user).size()>0)
		{
			logger.info("Login Service updating Token for user "+user.getEmail());
			loginRepository.updateToken(user);
		}
		else {
			logger.info("Login Service adding user "+user.getEmail());
			loginRepository.addUser(user);
		}
		logger.info("Login Service getting user details for "+user.getEmail());
		return loginRepository.getUser(user).get(0);
	}

}
