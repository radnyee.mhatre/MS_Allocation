package com.backend.msallocation.repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
//import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.backend.msallocation.constants.Queries;
import com.backend.msallocation.model.Skill;
import com.backend.msallocation.rowMapper.SkillsRowMapper;

@Repository
public class SkillRepository {
	Logger logger = LoggerFactory.getLogger(SkillRepository.class);
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	public void addSkill(Skill skill) {
		logger.info("Skill Repository adding skill "+skill.getSkillName());
		Date date = Calendar.getInstance().getTime();  
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("skillName", skill.getSkillName())
										.addValue("createdOn", date).addValue("createdBy", skill.getCreatedBy());
		jdbcTemplate.update(Queries.INSERT_IN_SKILL, parameters);		
	}
	public List<Skill> getSkills()
	{
		logger.info("Skill Repository get all skills ");
		return jdbcTemplate.query(Queries.GET_SKILLS_LIST, SkillsRowMapper.skillsRowMapperLambda);
	}
	
	public void addgradSkill(int gradId,Skill skills)
	{
		logger.info("Skill Repository adding skill "+skills.getSkillName()+" for grad id"+gradId);
		Date date = Calendar.getInstance().getTime();
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("gid", gradId)
				.addValue("sid", skills.getSkillID()).addValue("addedOn", date);
		jdbcTemplate.update(Queries.INSERT_GRAD_SKILL, parameters);
	}
	
	public List<Skill> getSkills(int gradId)
	{
		logger.info("Skill Repository get skills for grad id "+gradId);
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("gradId", gradId);
		return jdbcTemplate.query(Queries.GET_GRAD_SKILLS,parameters, SkillsRowMapper.skillsRowMapperLambda);
	}
	public void deletegrad(int gradId) {
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("gradId", gradId);
		jdbcTemplate.update(Queries.DELETE_FROM_GRAD_SKILLS, parameters);
	}

}
