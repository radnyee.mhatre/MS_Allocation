package com.backend.msallocation.rowMapper;

import org.springframework.jdbc.core.RowMapper;

import com.backend.msallocation.constants.Queries;
import com.backend.msallocation.model.Skill;

public class SkillsRowMapper {
	
	public static final RowMapper<Skill> skillsRowMapperLambda=(rs,rownum)-> {
		Skill skill=new Skill();
		skill.setSkillID(rs.getInt(Queries.SKILL_ID));
		skill.setSkillName(rs.getString(Queries.SKILL_NAME));
		skill.setCreatedOn(rs.getDate(Queries.CREATED_ON));
		skill.setCreatedBy(rs.getString(Queries.CREATED_BY));
        return skill;
	};

}
