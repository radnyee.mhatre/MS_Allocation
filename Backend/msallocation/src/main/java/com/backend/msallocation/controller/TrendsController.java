package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;

import com.backend.msallocation.model.Grad;
import com.backend.msallocation.model.LocationTrend;
import com.backend.msallocation.model.SkillMap;
import com.backend.msallocation.model.TrendYear;

public interface TrendsController {

	public ResponseEntity<?> getTrends(String idToken) throws GeneralSecurityException, IOException;
	public ResponseEntity<?> getSkillsTrends(String idToken) throws GeneralSecurityException, IOException;
}
