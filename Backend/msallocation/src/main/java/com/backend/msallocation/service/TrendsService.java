package com.backend.msallocation.service;

import java.util.List;
import java.util.Map;

import com.backend.msallocation.model.Grad;
import com.backend.msallocation.model.LocationTrend;
import com.backend.msallocation.model.SkillMap;
import com.backend.msallocation.model.TrendYear;

public interface TrendsService {
	public List<LocationTrend> getTrends();
	public List<SkillMap> getSkilTrend();
}
