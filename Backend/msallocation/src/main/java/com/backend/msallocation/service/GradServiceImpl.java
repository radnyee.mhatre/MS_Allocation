package com.backend.msallocation.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.backend.msallocation.model.Grad;
import com.backend.msallocation.model.Skill;
import com.backend.msallocation.repository.GradRepository;
import com.backend.msallocation.repository.SkillRepository;
import com.backend.msallocation.repository.TrendsRepository;

@Service
public class GradServiceImpl implements GradService {
	Logger logger = LoggerFactory.getLogger(GradServiceImpl.class);
	@Autowired
	GradRepository gradRepository;

	@Autowired
	SkillRepository skillrepository;

	@Override
	public Grad addGrad(Grad grad) {
		logger.info("Grad Service adding grad "+grad.getGradFirstName()+" "+grad.getGradLastName());
		int gradID = gradRepository.addGrad(grad);
		logger.info("Grad Service added Grad with id "+gradID+"\n Service Adding skills for grad id"+gradID);
		for (Skill skill : grad.getSkills()) {
			logger.info("Grad Service adding skill "+skill.getSkillID()+" for grad id "+gradID);
			skillrepository.addgradSkill(gradID, skill);
		}
		return gradRepository.getGrads(gradID).get(0);
	}

	@Override
	public Grad updateGrad(Grad grad) {
		logger.info("Grad Service updating grad id"+grad.getGradId());
		logger.info("Grad Service getting existing grad info for grad id"+grad.getGradId());
		Grad parent=gradRepository.getGrads(grad.getGradId()).get(0);
		gradRepository.addVersion(parent);
		gradRepository.updateGrad(grad);
		logger.info("Grad Service getting updated response for grad id"+grad.getGradId());
		Grad gradResponse = gradRepository.getGrad(grad.getGradId());
		gradResponse.setSkills(skillrepository.getSkills(grad.getGradId()));
		return gradResponse;
	}

	@Override
	public List<Grad> deleteGrad(int gradId) {
		gradRepository.deletegrad(gradId);
		return gradRepository.getGrads();

	}

	@Override
	public Grad getGrad(int grad_id) {
		logger.info("Grad Service getting details for grad id"+grad_id);
		List <Grad> gradResponse = gradRepository.getGrads(grad_id);
		if(gradResponse.size()>0) {
			gradResponse.get(0).setSkills(skillrepository.getSkills(grad_id));
			return gradResponse.get(0);
		}
		else return null;
		
	}

	@Override
	public List<Grad> getGrads() {
		// TODO Auto-generated method stub
		logger.info("Grad Service getting all grads");
		List<Grad> grads = gradRepository.getGrads();
		for (Grad grad : grads) {
			grad.setSkills(skillrepository.getSkills(grad.getGradId()));
		}
		return grads;
	}

	@Override
	public Grad addSkills(int gradId, List<Skill> skills) {
		logger.info("Grad Service Adding Skills for gradId : " +gradId);
		for(Skill skill:skills)
		{
			skillrepository.addgradSkill(gradId, skill);
		}
		return getGrad(gradId);
	}

	@Override
	public List<Grad> getHistory(int gradId) {
		logger.info("Grad Service getting History for gradId : " +gradId);
		return gradRepository.getHistory(gradId);
	}
	
}
