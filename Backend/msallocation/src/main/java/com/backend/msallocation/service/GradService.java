package com.backend.msallocation.service;

import java.util.List;

import com.backend.msallocation.model.Grad;
import com.backend.msallocation.model.Skill;

public interface GradService {
	public Grad addGrad(Grad grad);

	public Grad updateGrad(Grad grad);

	public List<Grad> deleteGrad(int grad_id);

	public Grad getGrad(int grad_id);

	public List<Grad> getGrads();
	
	public Grad addSkills(int gradId, List<Skill> skills);

	public List<Grad> getHistory(int gradId);

}
