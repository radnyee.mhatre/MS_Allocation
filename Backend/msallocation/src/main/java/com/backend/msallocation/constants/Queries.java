package com.backend.msallocation.constants;

public class Queries {
//	public static final String ="";

	// USER CONATANTS

	public static final String USER_EMAIL = "user_email";
	public static final String USER_NAME = "name";
	public static final String USER_TOKEN = "token";

	// GRAD TABLE CONSTANTS
	public static final String CREATED_BY = "created_by_user_email";
	public static final String CREATED_ON = "created_on";
	public static final String GRAD_ID = "grad_id";
	public static final String GRAD_FIRSTNAME = "grad_first_name";
	public static final String GRAD_MIDDLENAME = "grad_middle_name";
	public static final String GRAD_LASTNAME = "grad_last_name";
	public static final String GENDER = "grad_gender";
	public static final String EMAIL = "grad_email";
	public static final String CONTACT = "grad_contact";
	public static final String INSTITUTE = "grad_institute";
	public static final String DEGREE = "grad_degree";
	public static final String BRANCH = "grad_branch";
	public static final String DOJ = "joining_date";
	public static final String JOINING_LOCATION = "joining_location";
	public static final String FEEDBACK = "feedback";

	// SKILLS CONSTANTS
	public static final String SKILL_ID = "skill_id";
	public static final String SKILL_NAME = "skillname";
	public static final String ADDED_ON = "added_on";

	// ADDRESS CONSTANTS
	public static final String HOUSE_NO = "house_no";
	public static final String STREET = "street";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String ZIPCODE = "zipcode";

	public static final String PARENT_ID="parentID";
	public static final String LIVE="live";
	// INSERT IN GRAD TABLE
	public static final String INSERT_GRAD = "insert into grad_details(" + GRAD_FIRSTNAME + " , " + GRAD_MIDDLENAME
			+ " , " + GRAD_LASTNAME + " , " + GENDER + " , " + EMAIL + " , " + CONTACT + " , " + HOUSE_NO + " , "
			+ STREET + " , " + CITY + " , " + STATE + " , " + ZIPCODE + " , " + "" + INSTITUTE + " , " + DEGREE + " , "
			+ BRANCH + " , " + DOJ + " , " + JOINING_LOCATION + "," + CREATED_BY + " , " + CREATED_ON 
			+ ") values (:fname,:mname,:lname,:gender,:email,:contact,:houseNo,:street,:city,:state,:zipcode,"
			+ ":institute,:degree,:branch,:doj,:location,:createdBy,:createdOn)";
// VERSION INSERT
	public static final String INSERT_GRAD_VERSION = "insert into grad_details(" + GRAD_FIRSTNAME + " , " + GRAD_MIDDLENAME
			+ " , " + GRAD_LASTNAME + " , " + GENDER + " , " + EMAIL + " , " + CONTACT + " , " + HOUSE_NO + " , "
			+ STREET + " , " + CITY + " , " + STATE + " , " + ZIPCODE + " , " + "" + INSTITUTE + " , " + DEGREE + " , "
			+ BRANCH + " , " + DOJ + " , " + JOINING_LOCATION + "," + CREATED_BY + " , " + CREATED_ON +" , "+PARENT_ID+", "+LIVE
			+ ") values (:fname,:mname,:lname,:gender,:email,:contact,:houseNo,:street,:city,:state,:zipcode,"
			+ ":institute,:degree,:branch,:doj,:location,:createdBy,:createdOn,:parentId,1)";
	// UPDATE GRAD
	public static final String UPDATE_GRAD = "update grad_details set " + GRAD_FIRSTNAME + "=:fname , "
			+ GRAD_MIDDLENAME + "=:mname , " + GRAD_LASTNAME + "=:lname , " + GENDER + "=:gender , " + EMAIL
			+ "=:email , " + CONTACT + "=:contact , " + HOUSE_NO + "=:houseNo , " + STREET + "=:street , " + CITY
			+ "=:city , " + STATE + "=:state , " + ZIPCODE + "=:zipcode , " + FEEDBACK + " =:feedback , " + INSTITUTE
			+ "=:institute , " + DEGREE + "=:degree , " + BRANCH + "=:branch , " + DOJ + "=:doj , " + JOINING_LOCATION
			+ "=:location,"+CREATED_ON+"=:createdOn , "+ CREATED_BY+"=:createdBy where " + GRAD_ID
			+ "=:gradId";
	// GRAD BY ID
	public static final String GRAD_BY_ID = "select * from grad_details where " + GRAD_ID + " =:gradId";

	// DELETE GRAD
	public static final String DELETE_FROM_GRAD = "update grad_details set "+LIVE+"=1 where " + GRAD_ID + "=:gradId";

	// ALL GRADS
	public static final String ALL_GRADS = "select * from grad_details where "+PARENT_ID+"=0 and "+LIVE+"=0 order by "+GRAD_ID +" desc";

	// INSERT IN GARD_SKILLS
	public static final String INSERT_GRAD_SKILL = "insert into grad_skills(" + GRAD_ID + "," + SKILL_ID + ","
			+ ADDED_ON + ") values(:gid,:sid,:addedOn)";

	// INSERT IN SKILL
	public static final String INSERT_IN_SKILL = "insert into skills(" + SKILL_NAME + " , " + CREATED_ON + " , "
			+ CREATED_BY + ") values" + "(:skillName,:createdOn,:createdBy)";

	// FETCH SKILLS LIST
	public static final String GET_SKILLS_LIST = "select * from skills order by " + SKILL_NAME;

	// SKILL BY GRAD ID
	public static final String GET_GRAD_SKILLS = "select * from skills ,grad_skills where skills." + SKILL_ID
			+ "= grad_skills." + SKILL_ID + " and " + "grad_skills." + GRAD_ID + " = :gradId";

	// DELETE FROM GRAD SKILLS
	public static final String DELETE_FROM_GRAD_SKILLS = "delete from grad_skills where " + GRAD_ID + "=:gradId";

	// INSERT IN USER TABLE
	public static final String INSERT_IN_USER = "insert into users(" + USER_EMAIL + " , " + USER_NAME + " , "
			+ USER_TOKEN + ") " + "values (:email,:name,:token)";
	// CHECK USER IN LOGIN TABLE
	public static final String GET_LOGIN_USER = "select * from users where " + USER_EMAIL + " =:email";

	// UPDATE_USER_TOKEN
	public static final String UPDATE_TOKEN = "update users set " + USER_TOKEN + " =:token where " + USER_EMAIL
			+ " = :email ";

	// TRENDS
	public static final String DISTINCT_LOCATIONS = "select distinct " + JOINING_LOCATION + " from grad_details";

	public static final String COUNT_GRADS = "select count(*) from grad_details where " + DOJ + ">= :startDate and "
			+ DOJ + "< :endDate and " + JOINING_LOCATION + "=:location";
	public static final String SKILL_COUNTS = "select  count(*) from grad_skills where " + SKILL_ID + "=:skillId";
	
	
	//HISTORY
	public static final String GET_HISTORY="select * from grad_details where "+PARENT_ID+" = :gradId order by "+CREATED_ON+" desc";
}
