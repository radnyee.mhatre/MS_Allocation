package com.backend.msallocation.model;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Skill {
	private String createdBy;
	private Date createdOn;
	private int skillID;	
	private String SkillName;
	@Override
	public String toString() {
		return "Skill [createdBy=" + createdBy + ", createdOn=" + createdOn + ", skillID=" + skillID + ", SkillName="
				+ SkillName + "]";
	}
	
}
