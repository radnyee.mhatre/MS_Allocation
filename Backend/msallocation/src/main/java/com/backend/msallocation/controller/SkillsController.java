package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.backend.msallocation.model.Skill;

public interface SkillsController {
	
	public ResponseEntity<?> addSkill(String idToken,Skill skill) throws GeneralSecurityException, IOException;
	
	public ResponseEntity<?> getSkills(String idToken) throws GeneralSecurityException, IOException;

}
