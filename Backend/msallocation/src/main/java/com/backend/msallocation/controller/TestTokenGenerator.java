package com.backend.msallocation.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/api")
public class TestTokenGenerator {
	private MockMvc mockMvc;
	
	@GetMapping("/token")
	public String GetToken() throws Exception
	{
		 MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		    params.add("grant_type", "password");
		    params.add("client_id", "RxWM_O8kxpu4F9Q1mS3H39T4");
		    params.add("username", "radnyee.shailendramhatre@accoliteindia.com");
		    params.add("password", "Radnyee@17");
//		    
		    ResultActions result 
		      = mockMvc.perform(post("/oauth/token")
		        .params(params)
		        .with(httpBasic("RxWM_O8kxpu4F9Q1mS3H39T4","secret"))
		        .accept("application/json;charset=UTF-8"))
		        .andExpect(status().isOk())
		        .andExpect(content().contentType("application/json;charset=UTF-8"));
		 
//		    String resultString = result.andReturn().getResponse().getContentAsString();
//		 
//		    JacksonJsonParser jsonParser = new JacksonJsonParser();
//		    return jsonParser.parseMap(resultString).get("access_token").toString();
		    return "idToken";
	}
	
}
