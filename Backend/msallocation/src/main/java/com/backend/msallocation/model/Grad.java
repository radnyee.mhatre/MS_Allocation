package com.backend.msallocation.model;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Grad {

	@Override
	public String toString() {
		return "Grad [createdBy=" + createdBy + ", createdOn=" + createdOn + ", gradId=" + gradId + ", gradFirstName="
				+ gradFirstName + ", gradMiddleName=" + gradMiddleName + ", gradLastName=" + gradLastName + ", gender="
				+ gender + ", email=" + email + ", contact=" + contact + ", houseNo=" + houseNo + ", street=" + street
				+ ", city=" + city + ", state=" + state + ", zipcode=" + zipcode + ", institute=" + institute
				+ ", degree=" + degree + ", branch=" + branch + ", joiningDate=" + joiningDate + ", joiningLocation="
				+ joiningLocation + ", feedback=" + feedback + ", skills=" + skills + "]";
	}

	private String createdBy;
	private Date createdOn;
	private int gradId;
	private String gradFirstName;
	private String gradMiddleName;
	private String gradLastName;
	private String gender;
	private String email;
	private long contact;
	private String houseNo;
	private String street;
	private String city;
	private String state;
	private String zipcode;
	private String institute;
	private String degree;
	private String branch;
	private Date joiningDate;
	private String joiningLocation;
	private String feedback;
	private List<Skill> skills;
}
