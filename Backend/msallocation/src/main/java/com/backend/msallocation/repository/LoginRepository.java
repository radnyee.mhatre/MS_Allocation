package com.backend.msallocation.repository;

import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.backend.msallocation.constants.Queries;
import com.backend.msallocation.dto.LoginUser;
import com.backend.msallocation.rowMapper.UserRowMapper;

@Repository
public class LoginRepository {
	Logger logger = LoggerFactory.getLogger(LoginRepository.class);
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	public LoginRepository (DataSource dataSource) {
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public void addUser(LoginUser user) {
		logger.info("Login Repository adding new user"+user.getEmail());
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("email", user.getEmail()).addValue("name", user.getName())
				.addValue("token", user.getToken());
		int success=jdbcTemplate.update(Queries.INSERT_IN_USER, parameters);
	}
	
	public List<LoginUser> getUser(LoginUser user)
	{
		logger.info("Login Repository getting user"+user.getEmail());
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("email", user.getEmail());
		List<LoginUser> userList= jdbcTemplate.query(Queries.GET_LOGIN_USER, parameters, UserRowMapper.loginRowMapperLambda);
		
		return userList;
	}
	
	public void updateToken(LoginUser user)
	{
		logger.info("Login Repository updating token "+user.getToken()+" for user "+user.getEmail());
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("email", user.getEmail())
				.addValue("token", user.getToken());
		jdbcTemplate.update(Queries.UPDATE_TOKEN, parameters);
	}
}
