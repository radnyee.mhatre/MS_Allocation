package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.backend.msallocation.model.Grad;
import com.backend.msallocation.model.Skill;

public interface GradController {
	public ResponseEntity<?> addGrad( String idToken,Grad grad) throws GeneralSecurityException, IOException;
	
	public ResponseEntity<?> updateGrad( String idToken,Grad grad) throws GeneralSecurityException, IOException;
	
	public ResponseEntity<?> deleteGrad( String idTokenn, Integer grad_id) throws GeneralSecurityException, IOException;
	
	public ResponseEntity<?> getGrad( String idToken,Integer grad_id) throws GeneralSecurityException, IOException;
	
	public ResponseEntity<?> getGrads(String idToken) throws GeneralSecurityException, IOException;
	
	public ResponseEntity<?> addSkills(String idToken,Integer grad_id , List<Skill> skills) throws GeneralSecurityException, IOException;

	public ResponseEntity<?> gethistory(String idToken,int gradId) throws GeneralSecurityException, IOException;
}
