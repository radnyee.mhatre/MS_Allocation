package com.backend.msallocation.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.msallocation.model.Grad;
import com.backend.msallocation.model.LocationMap;
import com.backend.msallocation.model.LocationTrend;
import com.backend.msallocation.model.Skill;
import com.backend.msallocation.model.SkillMap;
import com.backend.msallocation.model.TrendYear;
import com.backend.msallocation.model.YearMap;
import com.backend.msallocation.repository.GradRepository;
import com.backend.msallocation.repository.SkillRepository;
import com.backend.msallocation.repository.TrendsRepository;

@Service
public class TrendsServiceImpl implements TrendsService {

	@Autowired
	GradRepository gradRepository;
	@Autowired
	TrendsRepository trendsRepository;
	@Autowired
	GradService gradService;
	@Autowired 
	SkillRepository skillRepository;
	@Override
	public List<LocationTrend> getTrends() {
		// TODO Auto-generated method stub
		
		List<Grad> gradList = gradService.getGrads();
		//return getTrendsYear(gradList);
		return getTrendsLocation(gradList);
		
	}


List<LocationTrend> getTrendsLocation(List<Grad> gradList) {
		
		Map<Integer, Integer> trends = new HashMap<Integer, Integer>();
		for (Grad grad : gradList) {
			int year = Integer.parseInt(grad.getJoiningDate().toString().substring(0, 4));
			
			if (!trends.containsKey(year)) {				
				trends.put(year,1);
			}
		}
		
        List<Integer> yearList=new LinkedList<Integer>();
        trends.forEach((key, value) -> yearList.add(key));
        List<String> locations=trendsRepository.getLocations();
        List<LocationTrend> trendList=new LinkedList<LocationTrend>();
        for(String location : locations) {
        	LocationTrend locationTrend=new LocationTrend(); 
        	List<YearMap> yearTrend=new LinkedList<YearMap>();
        	int locationCount=0;
        	for(int year : yearList)
        	{         
        		YearMap trend=new YearMap();
            	String startDate=year+"-01-01";
            	String endDate=(year+1)+"-01-01";
        		int count=trendsRepository.getCount(startDate, endDate, location);
        		trend.setCount(count);
        		trend.setYear(Integer.toString(year));
        		yearTrend.add(trend);
        		
        		locationCount+=count;
        	}
        	locationTrend.setYearMap(yearTrend);
        	locationTrend.setLocation(location);
        	locationTrend.setCount(locationCount);
        	trendList.add(locationTrend);
        }
		return trendList;
	}

@Override
public List<SkillMap> getSkilTrend() {
	List<Skill> skillList=skillRepository.getSkills();
	List<SkillMap> skillTrend=new LinkedList<SkillMap>();
	for(Skill skill:skillList) {
		SkillMap trend=new SkillMap();
		trend.setSkill(skill.getSkillName());
		trend.setCount(trendsRepository.getSkillCount(skill.getSkillID()));
		skillTrend.add(trend);
	}
	return skillTrend;
}

}