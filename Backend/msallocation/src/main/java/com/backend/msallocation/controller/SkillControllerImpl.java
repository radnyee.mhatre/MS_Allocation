package com.backend.msallocation.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.msallocation.model.Skill;
import com.backend.msallocation.service.SkillService;
import com.backend.msallocation.validator.TokenValidator;

@RestController
@RequestMapping("/api")
public class SkillControllerImpl implements SkillsController{
	Logger logger = LoggerFactory.getLogger(SkillControllerImpl.class);
	@Autowired
	SkillService skillService;
	TokenValidator validator=new TokenValidator();
	@PostMapping("/addSkill")
	@Override
	public ResponseEntity<?> addSkill(@RequestHeader("idToken")String idToken,@RequestBody Skill skill) throws GeneralSecurityException, IOException {
		logger.info("Skill Controller Validating login token :"+idToken);
		if(validator.validateToken(idToken)) {
			logger.info("Skill Controller Valid login token :"+idToken);
		logger.info("Skill Controller Adding Skill :"+skill.getSkillName());
		return new ResponseEntity<>(skillService.addSkill(skill), HttpStatus.OK); 
		}
		else 
		{
			logger.info("Skill Controller invalid token :"+idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);		}
	}
	
	@GetMapping("/skills")
	@Override
	public ResponseEntity<?> getSkills(@RequestHeader("idToken")String idToken) throws GeneralSecurityException, IOException {
		logger.info("Skill Controller Validating login token :"+idToken);
		if(validator.validateToken(idToken)) {
			logger.info("Skill Controller Valid login token :"+idToken);
		logger.info("Skill Controller Get all Skills");
		return new ResponseEntity<>(skillService.getSkills(), HttpStatus.OK); 
		}
		else 
		{
			logger.info("Skill Controller invalid token :"+idToken);
			return new ResponseEntity<>( new Error("Unauthorized User"), HttpStatus.UNAUTHORIZED);		}
	}

}
