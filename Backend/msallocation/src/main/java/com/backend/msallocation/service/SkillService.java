package com.backend.msallocation.service;

import java.util.List;

import com.backend.msallocation.model.Skill;

public interface SkillService {

	public List<Skill> addSkill(Skill skill);

	public List<Skill> getSkills();
}
