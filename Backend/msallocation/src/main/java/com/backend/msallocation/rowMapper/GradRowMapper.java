package com.backend.msallocation.rowMapper;

import org.springframework.jdbc.core.RowMapper;

import com.backend.msallocation.constants.Queries;
import com.backend.msallocation.model.Grad;


public class GradRowMapper {
	public static final RowMapper<Grad> gradRowMapperLambda=(rs,rownum)-> {
		Grad grad=new Grad();
		grad.setBranch(rs.getString(Queries.BRANCH));
		grad.setCity(rs.getString(Queries.CITY));
		grad.setContact(rs.getLong(Queries.CONTACT));
		grad.setCreatedBy(rs.getString(Queries.CREATED_BY));
		grad.setCreatedOn(rs.getDate(Queries.CREATED_ON));
		grad.setDegree(rs.getString(Queries.DEGREE));
		grad.setEmail(rs.getString(Queries.EMAIL));
		grad.setFeedback(rs.getString(Queries.FEEDBACK));
		grad.setGender(rs.getString(Queries.GENDER));
		grad.setGradFirstName(rs.getString(Queries.GRAD_FIRSTNAME));
		grad.setGradId(rs.getInt(Queries.GRAD_ID));
		grad.setGradLastName(rs.getString(Queries.GRAD_LASTNAME));
		grad.setGradMiddleName(rs.getString(Queries.GRAD_MIDDLENAME));
		grad.setHouseNo(rs.getString(Queries.HOUSE_NO));
		grad.setInstitute(rs.getString(Queries.INSTITUTE));
		grad.setJoiningDate(rs.getDate(Queries.DOJ));
		grad.setJoiningLocation(rs.getString(Queries.JOINING_LOCATION));
		grad.setState(rs.getString(Queries.STATE));
		grad.setStreet(rs.getString(Queries.STREET));
		grad.setZipcode(rs.getString(Queries.ZIPCODE));
		return grad;
	};
}
