package com.backend.msallocation.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.msallocation.model.Skill;
import com.backend.msallocation.repository.SkillRepository;

@Service
public class SkillServiceImpl implements SkillService  {
	Logger logger = LoggerFactory.getLogger(SkillServiceImpl.class);
	
	@Autowired
	SkillRepository skillRepository;
	
	@Override
	public List<Skill> addSkill(Skill skill) {
		logger.info("Skill Service adding skill "+skill.getSkillName());
		skillRepository.addSkill(skill);
		return skillRepository.getSkills();
	}

	@Override
	public List<Skill> getSkills() {
		logger.info("Skill Service get all skills");
		// TODO Auto-generated method stub
		return skillRepository.getSkills();
	}

}
