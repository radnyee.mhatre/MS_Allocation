create table if not exists users(user_email varchar(255) primary key,name varchar(255)not null, token varchar(255) not null,parentId int not null default(0));

create table if not exists grad_details (
grad_id int primary key auto_increment,
created_by_user_email varchar(255) references users(user_email),
created_on date not null,
grad_first_name varchar(50) not null,
grad_middle_name varchar(50) not null,
grad_last_name varchar(50) not null,
grad_gender varchar(10) not null,
grad_email varchar(255) not null,
grad_contact varchar(10) not null,
house_no varchar(50) not null,
street varchar(50) not null,
city varchar(50) not null,
state varchar(50)not null,
zipcode varchar(6)not null,
grad_institute varchar(50) not null,
grad_degree varchar(50) not null,
grad_branch varchar(20) not null,
joining_date date  not null,
joining_location varchar(50) not null,
feedback varchar(255),
parentId int not null default(0),
live int default(0)
);

create table if not exists skills(
skill_id int primary key auto_increment,
skillname varchar(50) not null,
created_by_user_email varchar(255) references users(user_email),
created_on date not null
);


create table if not exists grad_skills(
grad_id int references grad_details(grad_id),
skill_id int references skills(skill_id),
added_on date not null
);